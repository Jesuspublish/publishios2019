#!/bin/sh
pushd "`pwd`"
PWD_ANTERIOR=`pwd`

env > ~/Documents/mienv.txt

DESTINO_BUNDLE="/$PWD_ANTERIOR/../Publish88/Publish88/Publish88Resources.bundle"
UBICACION_BUNDLE="$CONFIGURATION_BUILD_DIR/Publish88Resources.bundle"

cd "$BUILD_ROOT"
echo "Ubicacion bundle $UBICACION_BUNDLE"
echo "Destino bundle   $DESTINO_BUNDLE"

#mkdir -p "$CODESIGNING_FOLDER_PATH/Publish88Resources.bundle"

xcodebuild -project "/$PWD_ANTERIOR/../Publish88/Publish88.xcodeproj" -target "Publish88Resources" -sdk "$PLATFORM_NAME" -configuration "$CONFIGURATION" CODE_SIGNING_REQUIRED=NO build SYMROOT="$SYMROOT"
#cp -R "$UBICACION_BUNDLE" "$DESTINO_BUNDLE"

popd

# Copiar archivos de carpeta de recursos al Bundle
if [ -d "$SRCROOT/Resources/$PRODUCTO/" ]; then
for ARCHIVO in "$SRCROOT/Resources/$PRODUCTO/"*
do
if [ -f "$ARCHIVO" ]; then
cp -f "$ARCHIVO" "$UBICACION_BUNDLE"
#"$TARGET_BUILD_DIR/$CONTENTS_FOLDER_PATH/Publish88Resources.bundle"
fi
done
fi

# Copiar el bundle completo dentro del .app
rm -R "$DESTINO_BUNDLE"
cp -f -R "$UBICACION_BUNDLE" "$DESTINO_BUNDLE"

# Copiar los recursos de primer nivel dentro del .app
if [ -d "$SRCROOT/Resources/$PRODUCTO/App/" ]; then
for ARCHIVO in "$SRCROOT/Resources/$PRODUCTO/App/"*
do
if [ -f "$ARCHIVO" ]; then
cp "$ARCHIVO" "$CODESIGNING_FOLDER_PATH"
fi
done
fi




